# Execution API Documentation

## General

Execution API provides endpoints for multiple execution contexts:

* Dispel4py: dynamically creates containers to execute Dispel4py workflows
* Specfem: creates containers in a dynamic way to execute Specfem executable
* Cyclone: to be created

## API calls

* **create-folders:** endpoint to be used by a new user to generate his/her workspace (folders).
The folder structure follows the below logic:
    - All folders are stored in the shared file system: .i.e. /home/mpiuser/sfs/
    - Inside the above folder, a new directory is generated based on the given username, 
    i.e. /home/mpiuser/sfs/username/
    - The user's folder contains: one folder for uploads (named uploads), one folder for the
    execution (named runs) and one folder for the test executions (named debug).
* **run-d4p:** endpoint to execute dispel4py workflows. It is based on a docker image (can be found in 
DARE platform repository, named exec-context-d4p), which creates new containers that execute the given
workflow. Data to be provided:
    - PE implementation id
    - Workspace id
    - package name
    - PE name
    - number of containers to be created
    - username
    - requirements: txt file containing the requirements of the PE
    - token for authentication
* **run-specfem:** endpoint to execute specfem software. The containers are generated to execute it and
are based in a docker image, which can be found in the platform (exec-context-specfem). Data to be
provided:
    - PE implementation id
    - Workspace id
    - package name
    - PE name
    - username
    - token for authentication
    - output filename
    - requirements: txt file containing the requirements of the PE
 * **run-cyclone:**
 * **upload:** API endpoint to upload files in the DARE platform. All uploaded files are stored in the
 uploads directory of each user (e.g. "../username/uploads")
 * **my-files:** API endpoint to list all the folders inside the three main directories of a user. The
 user directory is named after its username and inside it the three directories are: uploads, runs and
 debug (which is used for testing purposes)
 * **list:** API endpoint which lists all the files inside a specific directory. Use the previous endpoint
 to get the specific directory you need and then use this endpoint to view the files inside the directory.
 * **download:** API endpoint, used to download a specific file. Use the previous endpoint to get the 
 desired file and then use this endpoint to download it locally.
 * **send2drop:** API endpoint which uploads a file in B2Drop
 * **my-pods:** API endpoint to list all the execution containers related to a specific MPI job
