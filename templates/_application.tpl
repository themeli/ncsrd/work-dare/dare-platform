{{- define "dare.application.labels" -}}
app.kubernetes.io/name: {{ .Release.Name }}
{{- end -}}

{{- define "dare.standardLabels" -}}
app: {{ template "dare.name" . }}
chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
release: {{ .Release.Name }}
heritage: {{ .Release.Service }}
{{ if .Values.global.application.create -}}
{{ include "dare.application.labels" . }}
{{- end -}}
{{- end -}}

{{- define "dare.immutableLabels" -}}
app: {{ template "dare.name" . }}
chart: {{ .Chart.Name }}
release: {{ .Release.Name }}
heritage: {{ .Release.Service }}
{{ if .Values.global.application.create -}}
{{ include "dare.application.labels" . }}
{{- end -}}
{{- end -}}
