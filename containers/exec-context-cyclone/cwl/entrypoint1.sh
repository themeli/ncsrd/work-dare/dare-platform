#!/bin/bash
#

if [ -z "$RUN_DIR" ]
then
  RUN_DIR=$$
fi
  
SPROV_IMPORT_ENDPOINT="https://platform.dare.scai.fraunhofer.de/prov/workflowexecutions/import"
PROVENANCE_DIR="PROVENANCE"

EXEC_PATH="/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}"
cd ${EXEC_PATH}

mkdir ${EXEC_PATH}/cyclone_workflow_git
cp -r ${HOME}/cyclone_workflow_git/* ${EXEC_PATH}/cyclone_workflow_git/.

echo $pwd

cp ${EXEC_PATH}/cyclone_workflow_git/*.yml ${EXEC_PATH}/.
cp ${EXEC_PATH}/cyclone_workflow_git/*.cwl ${EXEC_PATH}/.
cp ${EXEC_PATH}/cyclone_workflow_git/*.sh ${EXEC_PATH}/.

# Run the cwl workflow with creating provenance
[ -z "${USER_ID}" ] && USER_ID=`whoami`
[ -z "${ISSUER}" ] && ISSUER="dare"
cwl-runner --provenance ${PROVENANCE_DIR} --enable-user-provenance  --enable-host-provenance --full-name ${USER_ID}@${ISSUER} --cachedir .cache tracking_master.cwl tracking_master_1.yml

# Send the created provenance to sprov
RESULT_LOCATION=`pwd`       # For now the result location is the current directory. This should be changed to an URL.

PROVENANCE_ZIP=${PROVENANCE_DIR}.zip
zip  -r ${PROVENANCE_ZIP} PROVENANCE/metadata/provenance

curl -X POST ${SPROV_IMPORT_ENDPOINT} -H "accept: application/json" -H "Content-Type: multipart/form-data" \
     -F "archive=@${PROVENANCE_ZIP};type=application/zip" -F "format=CWLProv" -F "resultLocation=$RESULT_LOCATION" \
     -F "runId=${RUN_ID}" -H "Authorization: OAuth ${ACCESS_TOKEN}"
#rm -f ${PROVENANCE_ZIP}

cp ${PROVENANCE_ZIP} /home/mpiuser/sfs/cpage/runs/${RUN_DIR}/.

ls -alF ${EXEC_PATH}
