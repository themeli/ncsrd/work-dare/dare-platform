cwlVersion: v1.0
class: CommandLineTool
baseCommand: [mpiexec]

inputs:
- id: no_processes
  type: int
  inputBinding:
    prefix: -n
    position: 1

- id: run_id
  type: string
  inputBinding:
    prefix: -x
    position: 2

- id: run_path
  type: string?
  inputBinding:
    prefix: -x
    position: 3

- id: d4py
  type: string
  inputBinding:
    position: 4

- id: target
  type: string
  inputBinding:
    position: 6

- id: workflow 
  type: string
  inputBinding:
    position: 7

- id: attribute
  type: Any?
  inputBinding:
    prefix: -a
    position: 5

- id: inputfile
  type: string? 
  inputBinding:
    prefix: -f
    position: 5

- id: inputdata
  type: Any?
  inputBinding:
    prefix: -d
    position: 5

- id: iterations
  type: int?
  inputBinding:
    prefix: -i
    position: 5

  ## This parameter must be supplied.
- id: provenance-config
  type: string
  inputBinding:
    prefix: --provenance-config=
    separate: false
    position: 5

  # This parameter could be optional, but made mandatory here, so we don't
  # have to rely on the user to provide it in the code.py file.
- id: userid
  type: string
  inputBinding:
    prefix: --provenance-userid=
    separate: false
    position: 5

  # This parameter must be supplied, but is more or less fixed and could
  # be hardcoded.
- id: provenance-repository-url
  type: string
  inputBinding:
    prefix: --provenance-repository-url=
    separate: false
    position: 5

  # This parameter must be supplied.
- id: provenance-bearer-token
  type: string
  inputBinding:
    prefix: --provenance-bearer-token=
    separate: false
    position: 5

  # This parameter needs to be supplied on MPI jobs and if there
  # is no runid in the configuration in code.py or in the file given as
  # value to the --provenance-config parameter. Not sure how to get CWL
  # into generating it though?
- id: provenance-runid
  type: string
  inputBinding:
    prefix: --provenance-runid=
    separate: false
    position: 5


outputs: []
