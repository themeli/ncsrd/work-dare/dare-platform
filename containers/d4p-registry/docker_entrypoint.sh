port=8000
dj_path=/registry

# Registry db initializations
until python $dj_path/manage.py makemigrations > /dev/null 2>&1 ;do
      echo "Waiting mysql docker to setup........"
      sleep 1
done

echo "Initializing....."
python $dj_path/manage.py makemigrations
python $dj_path/manage.py migrate
python $dj_path/manage.py migrate --run-syncdb

# Create superuser from localsettings
username="root"
password="root"
email=$username"@example.com"

echo "Creating super user....."

echo "from django.contrib.auth.models import User; User.objects.create_superuser('$username', '$email', '$password')" | python $dj_path/manage.py shell

echo "Fixtures...."

python $dj_path/manage.py loaddata $dj_path/fixtures/def_group.json

echo "Starting web server...."

# python $dj_path/manage.py runserver 0.0.0.0:$port
cd $dj_path
exec gunicorn  -w 9 -b 0.0.0.0:$port dj_vercereg.wsgi \
		--log-level debug \
                --backlog 0 \
                --timeout 120 
