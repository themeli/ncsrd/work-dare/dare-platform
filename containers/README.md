# Local Docker builds

Docker builds that are not pushed in a public registry
are built and pushed to `registry.gitlab.com/project-dare/dare-platform/`

