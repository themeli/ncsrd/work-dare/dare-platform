#! /bin/bash

set -e
OUTDIR="output"
EXEC_PATH="/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}"
cp cwl-files.py entrypoint.sh ./wp6_volcanology
cd wp6_volcanology

# find the workflow
python cwl-files.py
# execute the cwl workflow
cwl-runner --tmp-outdir-prefix="${EXEC_PATH}/${OUTDIR}" --tmpdir-prefix="${EXEC_PATH}/${OUTDIR}" "${WORKFLOW_NAME}" "${SPEC_NAME}"
