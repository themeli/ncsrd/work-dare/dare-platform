import json
import os
from os import getcwd
from os.path import join

import requests
import yaml

url = "http://" + os.environ["WORKFLOW_REGISTRY_PUBLIC_SERVICE_HOST"] + ":" + os.environ[
    "WORKFLOW_REGISTRY_PUBLIC_SERVICE_PORT"] + "/workflows/bynameversion"

data = {
    "workflow_name": os.environ["WORKFLOW_NAME"],
    "workflow_version": os.environ["WORKFLOW_VERSION"],
    "access_token": os.environ["ACCESS_TOKEN"]
}
response = requests.get(url, params=data)

if response.status_code == 200:
    resp = json.loads(response.text)

    with open(os.environ["WORKFLOW_NAME"], "w") as f:
        workflow = resp["workflow"]
        f.write(workflow)

    with open(resp["spec_name"], "w") as f:
        spec = resp["spec"]
        f.write(spec)
    with open(resp["spec_name"], "r") as f:
        spec = yaml.safe_load(f.read())
    with open(resp["spec_name"], "w") as f:
        spec["script"]["class"] = "File"
        spec["script"]["path"] = join(getcwd(), spec["script"]["path"])
        spec["access_token"] = os.environ["ACCESS_TOKEN"]
        # spec["delegation_token"] = os.environ["DELEGATION_TOKEN"]
        spec["issuer"] = os.environ["ISSUER"]
        spec["user_id"] = os.environ["USER_ID"]
        spec["run_id"] = os.environ["RUN_ID"]
        spec["prov_config"] = "inline"
        spec["prov_endpoint"] = "http://" + os.getenv("SPROV_SERVICE_HOST") + ':' + os.getenv("SPROV_SERVICE_PORT") + \
                                "/workflowexecutions/insert"
        yaml.safe_dump(spec, f)

    workflow_parts = resp["workflow_parts"] if "workflow_parts" in resp.keys() else None

    if workflow_parts:
        for workflow_part in workflow_parts:
            with open(workflow_part["name"], "w") as f:
                workflow_p = workflow_part["script"]
                f.write(workflow_p)
            if "spec_name" in workflow_part.keys() and workflow_part["spec_name"]:
                with open(workflow_part["spec_name"], "w") as f:
                    workflow_part_spec = workflow_part["spec_name"]
                    f.write(workflow_part_spec)
