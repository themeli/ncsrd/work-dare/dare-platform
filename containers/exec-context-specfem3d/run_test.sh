#!/bin/bash

echo "running example: `date`"
currentdir=`pwd`

# sets up directory structure in current example directory
echo
echo "   setting up example..."
echo

# stores setup
cp DATA/Par_file OUTPUT_FILES/specfem/
cp DATA/CMTSOLUTION OUTPUT_FILES/specfem/
cp DATA/STATIONS OUTPUT_FILES/specfem/

# get the number of processors, ignoring comments in the Par_file
NPROC=`grep ^NPROC DATA/Par_file | grep -v -E '^[[:space:]]*#' | cut -d = -f 2`

BASEMPIDIR=`grep ^LOCAL_PATH DATA/Par_file | cut -d = -f 2 `
echo $BASEMPIDIR
mkdir -p $BASEMPIDIR

# decomposes mesh using the pre-saved mesh files in MESH-default
echo
echo "  decomposing mesh..."
echo
xdecompose_mesh $NPROC ./DATA/mesh_Abruzzo $BASEMPIDIR
exit
# checks exit code
if [[ $? -ne 0 ]]; then exit 1; fi
# runs database generation
if [ "$NPROC" -eq 1 ]; then
  # This is a serial simulation
  echo
  echo "  running database generation..."
  echo
  xgenerate_databases
else
  # This is a MPI simulation
  echo
  echo "  running database generation on $NPROC processors..."
  echo
  mpirun --allow-run-as-root -np $NPROC xgenerate_databases
fi
# checks exit code
if [[ $? -ne 0 ]]; then exit 1; fi
# runs simulation
if [ "$NPROC" -eq 1 ]; then
  # This is a serial simulation
  echo
  echo "  running solver..."
  echo
  xspecfem3D
else
  # This is a MPI simulation
  echo
  echo "  running solver on $NPROC processors..."
  echo
  mpirun --allow-run-as-root -np $NPROC xspecfem3D
fi
# checks exit code
if [[ $? -ne 0 ]]; then exit 1; fi

echo
echo "see results in directory: OUTPUT_FILES/"
echo
echo "done"
echo `date`
