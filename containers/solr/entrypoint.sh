#! /bin/sh

bin/solr start
sleep 180

# create a core in solr
solr create_core -c dare

# configurations for geospacial data
wget http://central.maven.org/maven2/org/locationtech/jts/jts-core/1.15.0/jts-core-1.15.0.jar
mv jts-core-1.15.0.jar server/solr-webapp/webapp/WEB-INF/lib/