---
title: DARE platform v3.3 release
date: 2020-06-24
image: "images/dare_transparent.png"
---

DARE platform v3.3 release is now available at https://platform.dare.scai.fraunhofer.de/

Thanks to new use cases, we fixed multiple issues, especially regarding the CWL support. Visit our toy example [here](https://project-dare.gitlab.io/dare-platform/demo/)

To download the v3.3 Release of the DARE platform visit our 
[GitLab Repository](https://gitlab.com/project-dare/dare-platform/-/tree/v3.3)
 
