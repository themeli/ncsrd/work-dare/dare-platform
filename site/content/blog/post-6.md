---
title: DARE platform v3.2 release
date: 2020-05-25
image: "images/dare_transparent.png"
---

DARE platform v3.2 release is now available at https://testbed.project-dare.eu/

In the v3.2 release, we have completely integrated the Keycloak AAI to the Dispel4py Information Registry and performed
various updates in our Execution API. We have also deployed a new component, as a central login point, which handles
all the sign in actions and validates the provided access tokens. The dare-login component uses Keycloak as backend
to issue tokens and validate the users. The rest DARE components use the dare-login for all AAI actions.

The new DARE Login component is available [here](https://gitlab.com/project-dare/dare-login). As usual, we provide
client-side helper functions to interact with the component as well as a GitLab page for technical documentation.

To download the v3.2 Release of the DARE platform visit our 
[GitLab Repository](https://gitlab.com/project-dare/dare-platform/-/tree/v3.2)
 
