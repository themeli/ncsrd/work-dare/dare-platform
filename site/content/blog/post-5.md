---
title: DARE platform v3.1 release
date: 2020-05-04
image: "images/dare_transparent.png"
---

DARE platform v3.1 release is now available at https://testbed.project-dare.eu/

From this version, apart from dispel4py, we also support CWL workflows.
We have a new component, deployed in the platform, i.e. the workflow-registry. Domain developers
can register their docker containers and CWL workflows in the CWL Workflow Registry and refer to them
by name and version. For more information, check the technical documentation of the Workflow registry
[here](https://project-dare.gitlab.io/workflow-registry/). 

We provide client-side functions 
[here](https://gitlab.com/project-dare/workflow-registry/-/blob/master/workflow_client/helper_functions.py),
which wrap all the endpoints provided by the workflow-registry component. You can use these functions
to store, update, retrieve and delete dockers and workflows. Download and modify our demo jupyter notebook
from [here](https://gitlab.com/project-dare/workflow-registry/-/tree/master/workflow_client) in order to 
register your own dockers and workflows.

For the CWL execution, research developers can use the /run-cwl endpoint of the Execution API,
specifying the name and version of the workflow. Thus, the DARE platform will deploy your
execution environment and run your workflow. As usual, the logs and output files will be stored in the
platform's Shared File System.

To download the v3.1 Release of the DARE platform visit our 
[GitLab Repository](https://gitlab.com/project-dare/dare-platform/-/tree/v3.1)
 
