---
title: "API Documentation"
---

In this page, information on DARE API is provided. DARE Platform follows the Microservices software architecture, therefore the DARE API is
constitute by multiple individual APIs. The following sections list the API endpoints of each DARE service. For high level description of 
the components visit the features section of our site.

The main component APIs described here are:

1. dare-login service which interacts with Keycloak in the backend
2. d4p-registry, API to interact with the dispel4py workflow registry
3. workflow-registry, API to interact with the CWL workflow registry
4. exec-api, the DARE execution API
5. s-prov, the provenance API
6. playground, which enables a development environment for scientists to write their workflows
7. semantic-data-discovery, API to retrieve data from the Data Catalogue

Finally, we provide documentation on the dispel4py library.

## DARE Login API

DARE login service interacts between DARE components and Keycloak. It exposes functionality for sign in to the platform, refreshing and validating a token etc. The main API calls are:

<table class="table">
	
<thead>
	<tr>
		<th scope="col"><b>HTTP method</b></th>
        <th scope="col"><b>Endpoint</b></th>
        <th scope="col"><b>Description</b></th>
        <th scope="col"><b>Content Type</b></th>
        <th scope="col"><b>Parameters</b></th>
	</tr>
</thead>
	<tbody>
		<tr>
			<td>POST</td>
			<td>/auth/</td>
			<td>Authenticates a user performing HTTP call <br>to the Keycloak service. <br>After having successfully authenticated <br>the user, Dispel4py Registry, <br>CWL registry and Execution API are <br>notified to check if the <br>user already exists in their local DBs</td>
			<td>application/json</td>
			<td>data (body), example: <br>{ <br>"username": "string", <br>"password": "string", <br>"requested_issuer": "string"}</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/validate-token/</td>
			<td>Validates a token using the Keycloak Service</td>
			<td>application/json</td>
			<td>data (body),example: <br>{<br>"access_token": "string"<br>}</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/delegation-token/</td>
			<td>Issues a token for internal application use</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>"access_token": "string"<br>}</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/refresh-token/</td>
			<td>Uses the refresh token to issue a new token for a user</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>"refresh_token": "string",<br>"issuer": "string"<br>}</td>
		</tr>
	</tbody>
</table>

The technical documentation of the dare-login component can be found [here](https://project-dare.gitlab.io/dare-login/).

## D4p Information Registry

The Dispel4py Workflow Registry enables the research developers to register their dispel4py workflows, re-use and share them. The fowlloing table shows the available API endpoints of this component.

<table class="table">
<thead>
	<tr>
            <th scope="col"><b>HTTP method</b></th>
            <th scope="col"><b>Endpoint</b></th>
            <th scope="col"><b>Description</b></th>
            <th scope="col"><b>Content Type</b></th>
            <th scope="col"><b>Parameters</b></th>
        </tr>
</thead>
<tbody>
	<tr>
		<td>GET</td>
		<td>/connections/</td>
		<td>Retrieves all the available PE Connection resources. <br>A PE Connection resource allows the <br>addition and manipulation <br>of PE connections. <br>Connections are associated with PEs and <br>are not themselves workspace items</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/connections/</td>
		<td>Creates a new PE Connection resource, <br>which allows the addition <br>and manipulation of PE connections. <br>Connections are associated <br>with PEs and are not themselves <br>workspace items</td>
		<td>application/json</td>
		<td>data (body) example: {<br>  "comment" : "string" ,  <br>"kind" : "string" ,  <br>"modifiers" : "string" ,  <br>"name" : "string" ,  <br>"is_array" : true ,  <br>"s_type" : "string" ,  <br>"d_type" : "string" ,  <br>"pesig" : "string" }</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/connections/{id}/</td>
		<td>Retrieves a specific PE Connection resource. <br>A PE Connection resource allows the addition <br>and manipulation of PE connections. <br>Connections are associated with PEs and <br>are not themselves workspace items.</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/connections/{id}/</td>
		<td>Updates an existing PE Connetion resource. <Br>A PE Connection resource allows the addition <br>and manipulation of PE connections. <br>Connections are associated with PEs and <br>are not themselves workspace items.</td>
		<td>application/json</td>
		<td>-id (integer) <br>-data (body) example: <br>{<br>  "comment" : "string" ,  <br>"kind" : "string" ,  <br>"modifiers" : "string" ,  <br>"name" : "string" ,  <br>"is_array" : true ,  <br>"s_type" : "string" ,  <br>"d_type" : "string" ,  <br>"pesig" : "string" <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/connections/{id}/</td>
		<td>Deletes an existing PE Connection resource <br>from the DB</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/fnimpls/</td>
		<td>Retrieve all the available function <br>implementation resources</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/fnimpls/</td>
		<td>Creates a new Function Implementation</td>
		<td>application/json</td>
		<td>data (body), example: <br>{<br>"code" : "string", <br>"parent_sig": "string", <br>"description" : "string", <br>"pckg" : "string", <br>"workspace": "string" , <br>"clone_of": "string" , <br>"name": "string" <br>}</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/fnimpls/{id}/</td>
		<td>Retrieves a specific Function implementation resource</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/fnimpls/{id}/</td>
		<td>Updates an existing function implementation</td>
		<td>application/json</td>
		<td>-id (integer) <br>-data (body), example: <br>{ <br>"code": "string", <br>"parent_sig": "string", <br>"description": "string", <br>"pckg": "string", <br>"workspace": "string", <br>"clone_of": "string", <br>"name": "string" <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/fnimpls/{id}/</td>
		<td>Deletes an existing Function Implementation</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/fnparams/</td>
		<td>Retrieves all the available Function Parameters</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/fnparams/</td>
		<td>Creates a new Function Parameters</td>
		<td>application/json</td>
		<td>data (body), example: <br>{<br>"parent_function": "string", <br>"param_name": "string", <br>"param_type" : "string" <br>}</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/fnparams/{id}/</td>
		<td>Retrieves a specific Function Parameters</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/fnparams/{id}/</td>
		<td>Updates an existing Function <br>Parameters entry</td>
		<td>application/json</td>
		<td>-id (integer) <br>-data (body) example: <br>{<br>"parent_function": "string", <br>"param_name": "string", <br>"param_type": "string" <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/fnparams/{id}/</td>
		<td>Deletes an existing Function Parameters</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/functions/</td>
		<td>Retrieves all the Function resources <br>from the DB</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/functions/</td>
		<td>Creates a new Function Resource</td>
		<td>application/json</td>
		<td>data (body), example: <br>{ <br>"description" : "string", <br>"parameters" : ["string"], <br>"fnimpls": ["string"], <br>"pckg": "string", <br>"workspace": "string", <br>"return_type": "string", <br>"clone_of": "string", <br>"name": "string" <br>}</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/functions/{id}/</td>
		<td>Retrieves an existing Function resource</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/functions/{id}/</td>
		<td>Updates an existing function resouce</td>
		<td>application/json</td>
		<td>-id (integer) <br>-data(body), example: <br>{ <br>"description": "string", <br>"parameters": ["string"], <br>"fnimpls": ["string"], <br>"pckg": "string", <br>"workspace": "string", <br>"return_type": "string", <br>"clone_of": "string", <br>"name": "string" <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/functions/{id}/</td>
		<td>Deletes an existing function resource</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/groups/</td>
		<td>Retrieves all the available groups</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/groups/</td>
		<td>Creates a new user group</td>
		<td>application/json</td>
		<td>data (body), example: { "name": "string" }</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/groups/{id}/</td>
		<td>Retrieves a user group</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/groups/{id}/</td>
		<td>Updates an existing user group</td>
		<td>application/json</td>
		<td>-id (integer) <br>-data (body), <br>example: { "name": "string" }</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/groups/{id}/</td>
		<td>Removes a user group</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/literals/</td>
		<td>Retrieves all the literal entities</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/literals/</td>
		<td>Creates a new Literal Entities</td>
		<td>application/json</td>
		<td>data (body), example: <br>{ <br>"description": "string", <br>"value": "string", <br>"name": "string", <br>"pckg": "string", <br>"workspace": "string", <br>"clone_of" : "string" <br>}</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/literals/{id}/</td>
		<td>Retrieves a Literal Entities</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/literals/{id}/</td>
		<td>Updates an existing Literal Entities</td>
		<td>application/json</td>
		<td>-id (integer) <br>-data (body), example: <br>{ <br>"description": "string", <br>"value": "string", <br>"name": "string", <br>"pckg": "string", <br>"workspace": "string", <br>"clone_of": "string" <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/literals/{id}/</td>
		<td>Deletes a Literal Entities</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/peimpls/</td>
		<td>Retrieves all the available <br>PE Implementation</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/peimpls/</td>
		<td>Creates a new PE Implementation</td>
		<td>application/json</td>
		<td>data (body), example: <br>{ <br>"code": "string", <br>"parent_sig": "string", <br>"description": "string", <br>"pckg": "string", <br>"workspace": "string", <br>"clone_of": "string", <br>"name": "string" <br>}</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/peimpls/{id}/</td>
		<td>Retrieves a specific <br>PE Implementation</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/peimpls/{id}/</td>
		<td>Updates an existing PE Implementation</td>
		<td>application/json</td>
		<td>-id (integer) <br>-data(body), example: <br>{ <br>"code": "string", <br>"parent_sig": "string", <br>"description": "string", <br>"pckg": "string", "workspace": "string", <br>"clone_of" : "string", <br>"name": "string" <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/peimpls/{id}/</td>
		<td>Deletes an existing PE Implementation</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/pes/</td>
		<td>Retrieves all the available PE resources</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/pes/</td>
		<td>Creates a new PE</td>
		<td>application/json</td>
		<td>data (body), example: <br>{<br>"description": "string", <br>"name": "string", <br>"connections": ["string"], <br>"pckg": "string", <br>"workspace": "string", <br>"clone_of": "string", <br>"peimpls": ["string"] <br>}</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/pes/{id}/</td>
		<td>Retrieves a specific PE</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/pes/{id}/</td>
		<td>Updates an existing PE</td>
		<td>application/json</td>
		<td>-id(integer) <br>-data(body), example: <br>{ <br>"description": "string", <br>"name": "string", <br>"connections": ["string"], <br>"pckg": "string", <br>"workspace": "string", <br>"clone_of": "string", <br>"peimpls": ["string"] <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/pes/{id}/</td>
		<td>Deletes an existing PE</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/registryusergroups/</td>
		<td>Retrieves all the available <br>registry user groups</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/registryusergroups/</td>
		<td>Creates a new Registry user group</td>
		<td>application/json</td>
		<td>data (body), example: <br>{ <br>"description": "string", <br>"group_name": "string" <br>}</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/registryusergroups/{id}/</td>
		<td>Retrieves a specific Registry user group</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/registryusergroups/{id}/</td>
		<td>Updates an existing Registry user group</td>
		<td>application/json</td>
		<td>-id(integer) <br>-data(body), example: <br>{ <br>"owner": "string", <br>"description": "string", <br>"group_name": "string" <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/registryusergroups/{id}/</td>
		<td>Deletes an existing Registry user group</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/users/</td>
		<td>Retrieves all the existing users</td>
		<td>application/json</td>
		<td>No parameters</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/users/</td>
		<td>Creates a new user</td>
		<td>application/json</td>
		<td>data (body), example: <br>{ <br>"username": "string", <br>"password": "string", <br>"first_name": "string", <br>"last_name": "string", <br>"email": "string" <br>}</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/users/{id}/</td>
		<td>Retrieves a specific user</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/users/{id}/</td>
		<td>Updates a specific user</td>
		<td>application/json</td>
		<td>-id(integer) <br>-data(body), example: <br>{ <br>"username": "string", <br>"password": "string", <br>"first_name": "string", <br>"last_name": "string", <br>"email": "string" <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/users/{id}/</td>
		<td>Deletes a specific user</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>/workspaces/</td>
		<td>Retrieves all the available workspaces</td>
		<td>application/json</td>
		<td>parameters:<br> <br>-name: name <br>-description: The name of the workspace we want to display <br>-paramType: query <br><br>-name: username <br>-description: The username the workspace is associated with  <br>-paramType: query <br><br>-name: search <br>- description: perform a simple full-text on <br>descriptions and names of workspaces. <br>-paramType: query</td>
	</tr>
	<tr>
		<td>POST</td>
		<td>/workspaces/</td>
		<td>Create or clone a new workspace</td>
		<td>application/json</td>
		<td>parameters: <br> <br>- name: name <br>- description: the name of the workspace. <br><br>- name: description <br>- description: a textual description of the workspace. <br><br>- name: clone_of <br>- description: indicates that a cloning operation is requested. <br>- paramType: query <br>- type: long</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>Retrieves a specific workspace</td>
		<td></td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
	<tr>
		<td>PUT</td>
		<td>/workspaces/{id}/</td>
		<td>Updates an existing workspace</td>
		<td>application/json</td>
		<td>-id (integer) <br>-data (body), example: <br>{ <br>"clone_of": "string", <br>"name": "string", <br>"description": "string" <br>}</td>
	</tr>
	<tr>
		<td>DELETE</td>
		<td>/workspaces/{id}/</td>
		<td>Deletes an existing workspace</td>
		<td>application/json</td>
		<td>id (integer)</td>
	</tr>
</tbody>
</table>

## CWL Workflow Registry

This component is a Django Web Service exposing an API for CWLs and dockers registration. The technical documentation for the CWL workflow registry is available in the project's micro site [here](https://project-dare.gitlab.io/workflow-registry/). The following table shows all the available API calls in the CWL workflow registry.

<table>
	<thead>
		<tr>
	        <th scope="col"><b>HTTP method</b></th>
	        <th scope="col"><b>Endpoint</b></th>
	        <th scope="col"><b>Description</b></th>
	        <th scope="col"><b>Content Type</b></th>
	        <th scope="col"><b>Parameters</b></th>
	    </tr>
	</thead>
	<tbody>
		<tr>
			<td>POST</td>
			<td>/docker/</td>
			<td>Creates a new Docker Environment. <br>The environment consist of a Dockerfile and <br>can be associated with one or <br>multiple DockerScript entries <br>(which represent bash or python scripts)</td>
			<td>application/json</td>
			<td>data (body), example:<br>{<br>
        	"docker_name": "name",<br>
       		"docker_tag": "tag",<br>
      		"script_names": ["script1.sh", "script2.sh"]<br>
        	"files": <br>{<br>
            	"dockerfile": "string",<br>
            	"script1.sh": "string.",<br>
            	"script2.sh": "string"<br>
        	},<br>
        	"access_token": "token"<br>}
    		</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/docker/update_docker/</td>
			<td>Updates an existing Docker Environment</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"docker_name": "name",<br>
				"docker_tag" "tag",<br>
				"update": {"tag": "v2.0"},<br>
				"files": {"dockerfile": "string"},<br>
				"access_token": "token"<br>}
			</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/docker/provide_url/</td>
			<td>Updates an existing Docker environment’s url field. <br> Once the docker image is built <br>and pushed in a public repository, <br>the relevant Docker entry should <br>be updated with the URL.</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"docker_name": "name",<br>
				"docker_tag": "tag",<br>
				"docker_url": "url",<br>
				"access_token": "token"<br>}
			</td>
		</tr>
		<tr>
			<td>DELETE</td>
			<td>/docker/delete_docker/</td>
			<td>Deletes an existing docker environment.</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"docker_name": "name",<br>
				"docker_tag": "tag",<br>
				"access_token": "token"<br>}
			</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/docker/bynametag/</td>
			<td>Retrieves a Docker Environment using <br>its name and tag.</td>
			<td>application/json</td>
			<td>-docker_name (string)<br>-docker_tag(string)</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/docker/byuser/</td>
			<td>Retrieves all the registered Docker <br>environments by user</td>
			<td>application/json</td>
			<td>-requested_user(string) if exists, <br>otherwise it uses the user that <br>performed the request</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/docker/download/</td>
			<td>Downloads in a zip file the Dockerfile <br>and the relevant scripts of a <br>Docker Environment.</td>
			<td>application/json</td>
			<td>-docker_name (string)<br>-docker_tag(string)</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/scripts/add/</td>
			<td>Adds a new script in an existing <br>Docker Environment</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"docker_name": "name",<br>
				"docker_tag": "tag",<br>
				"script_name": "entrypoint.sh",<br>
				"files": {"entrypoint.sh": "string"},<br>
				"access_token": "token"<br>}
			</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/scripts/edit/</td>
			<td>Edits an existing script of a <br>Docker Environment</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"docker_name": "name",<br>
				"docker_tag": "tag",<br>
				"script_name": "entrypoint.sh",<br>
				"files": {"entrypoint.sh": “string”},<br>
				"access_token": "token"<br>}
			</td>
		</tr>
		<tr>
			<td>DELETE</td>
			<td>/scripts/delete/</td>
			<td>Deletes an existing script from a docker environment</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"docker_name": "name",<br>
				"docker_tag": "tag",<br>
				"script_name": "entrypoint.sh",<br>
				"access_token": "token"<br>}
			</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/scripts/download</td>
			<td>Downloads a specific script from <br>a Docker Environment</td>
			<td>application/json</td>
			<td>-docker_name(string)<br>-docker_tag(string)<br>-script_name(string)</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/scripts/byname</td>
			<td>Retrieves a specific script based on the name <br>& tag of the Docker Environment <br>and on the name of the script.</td>
			<td>application/json</td>
			<td>-docker_name(string)<br>-docker_tag(string)<br>-script_name(string)</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/workflows/</td>
			<td>Creates a new CWL workflow of <br>class Workflow</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"workflow_name": "demo_workflow.cwl",<br>
				"workflow_version": "v1.0",<br>
				"spec_file_name": "spec.yaml",<br>
				"docker_name": "name",<br>
				"docker_tag": "tag",<br>
				"workflow_part_data":<br>[{<br>"name":arguments.cwl”,<br>
				"version":"v1.0", <br>"spec_name": "arguments.yaml"<br>},<br>{<br>"name": "tar_param.cwl", <br>"version":"v1.0", <br>"spec_name": "tar_param.yaml"<br>}],<br>
				"files": <br>{<br>"demo_workflow.cwl":"string",<br>
				"spec.yaml": "string",<br>
				"arguments.cwl": "string",<br>
				"arguments.yaml": "string",<br>
				"tar_param.cwl": "string",<br>
				"tar_param.yaml": "string"<br>},<br>
				"access_token": "token"<br>
				}
			</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/workflows/update_workflow/</td>
			<td>Updates an existing CWL workflow of <br>class Workflow</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"workflow_name":"demo_workflow.cwl",<br>
				"workflow_version": "v1.0",<br>
				"files": {"workflow_file": "string",<br>
				"spec_file": "string",},<br>
				"update": {"version":"v1.1"},<br>
				"access_token": "token"<br>}
			</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/workflows/update_docker/</td>
			<td>Associate a CWL workflow of class <br>Workflow with a different Docker <br>Environment.</td>
			<td>application/json</td>
			<td>data (body), example:<br>
				{<br>
				"workflow_name":"demo_workflow.cwl",<br>
				"workflow_version": "v1.0",<br>
				"docker_name": "test",<br>
				"docker_tag": "v1.0",<br>
				"access_token": "token"<br>
				}
			</td>
		</tr>
		<tr>
			<td>DELETE</td>
			<td>/workflows/delete_workflow/</td>
			<td>Deletes an existing CWL workflow <br>(class Workflow) and all the <br>associated Workflow parts <br>(class CommandLineTool).</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"workflow_name":"demo_workflow.cwl",<br>
				"workflow_version": "v1.0",<br>
				"access_token": "token"<br>
				}
			</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/workflows/bynameversion/</td>
			<td>Retrieve a CWL workflow of class Workflow <br>and its associated workflow parts <br>as well as the related docker <br>environment, based on the workflow <br>name and version.</td>
			<td>application/json</td>
			<td>-workflow_name(string)<br>-workflow_version(string)</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/workflows/download</td>
			<td>Downloads in a zip file all the CWL files <br>(Workflow and CommandLineTool) as well as <br>the relevant Dockerfile and scripts <br>(if the parameter dockerized is provided)</td>
			<td>application/json</td>
			<td>-workflow_name(string)<br>-workflow_version(string)<br>-dockerized(boolean)</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td>application/json</td>
			<td></td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/workflow_parts/add/</td>
			<td>Adds a new CommandLineTool CWL in an existing CWL workflow</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"workflow_name":"demo_workflow.cwl",<br>
				"workflow_version": "v1.0",<br>
				"workflow_part_name":"arguments.cwl",<br>
				"workflow_part_version": "v1.0",<br>
				"spec_name": "arguments.yaml",<br>
				"files": {"arguments.cwl": "string",<br>
				"arguments.yaml": "string"},<br>
				"access_token": "token"<br>
				}
			</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/workflow_parts/edit/</td>
			<td>Edits an existing CommandLineTool CWL workflow</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"workflow_name":"demo_workflow.cwl",<br>
				"workflow_version": "v1.0",<br>
				"workflow_part_name":"arguments.cwl",<br>
				"workflow_part_version": "v1.0",<br>
				"spec_name": "arguments.yaml",<br>
				"files": <br>{<br>"arguments.cwl":"string",<br>
				"arguments.yaml": "string"<br>},<br>
				"update": <br>{<br>"version":"v1.1”<br>},<br>
				"access_token": "token"<br>
				}
			</td>
		</tr>
		<tr>
			<td>DELETE</td>
			<td>/workflow_parts/delete/</td>
			<td>Deletes an existing CommandLIneTool CWL workflow</td>
			<td>application/json</td>
			<td>data (body), example:
				<br>{<br>
				"workflow_name":"demo_workflow.cwl",<br>
				"workflow_version": "v1.0",<br>
				"workflow_part_name": "arguments.cwl",<br>
				"workflow_part_version": "v1.0",<br>
				"access_token": "token"<br>
				}
			</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/workflow_parts/bynameversion</td>
			<td>Retrieves a specific CommandLineTool CWL based <br>on its parent (name & version) and <br>its own name and version.</td>
			<td>application/json</td>
			<td>-workflow_name(string)<br>-workflow_version(string)<br>-workflow_part_name(string)<br>-workflow_part_version(string)</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/workflow_parts/download/</td>
			<td>Downloads a specific CWL of class CommandLineTool</td>
			<td>application/json</td>
			<td>-workflow_name(string)<br>-workflow_version(string)<br>-workflow_part_name(string)<br>-workflow_part_version(string)</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/accounts/login/</td>
			<td>Authenticates a user (login) used by the <br>dare-login component described above <br>when a user calls the /auth/ endpoint of the dare-login. <br>If the user does not exist in the CWL workflow <br>registry’s local DB, <br>it creates a new user.</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"username": "string",<br>
				"password":"string",<br>
				"access_token":"string",<br>
				"email":"string",<br>
				"given_name":"string",<br>
				"family_name":"string"<br>
				}
			</td>
		</tr>
	</tbody>
</table>

## Execution API

### General

Execution API provides endpoints for multiple execution contexts:

* Dispel4py: dynamically creates containers to execute Dispel4py workflows
* CWL: execution environments spawned on-demand to execute CWL workflows.
* Specfem: creates containers in a dynamic way to execute Specfem executable. This endpoint is deprecated, Specfem is now executed via the CWL endpoint.

### API calls

<table>
	<thead>
		<tr>
	        <th scope="col"><b>HTTP method</b></th>
	        <th scope="col"><b>Endpoint</b></th>
	        <th scope="col"><b>Description</b></th>
	        <th scope="col"><b>Content Type</b></th>
	        <th scope="col"><b>Parameters</b></th>
	    </tr>
	</thead>
	<tbody>
		<tr>
			<td>POST</td>
			<td>/create-folders/</td>
			<td>Endpoint used by the /auth/ endpoint of dare-login. <br>Checks if the user’s workspace in the DARE platform <br>is available, otherwise it creates the <br>necessary folder structure</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>	"username": "string"<br>}</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/d4p-mpi-spec/</td>
			<td>Used internally by the dispel4py execution environment <br>in order to retrieve the respective <br>PE Implementation and spec.yaml</td>
			<td>application/json</td>
			<td>data (body),example: <br>{<br>
				"pe_imple": "name",<br>
				"nodes": 3,<br>
				"input_data": {}<br>
				}
			</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/run-d4p/</td>
			<td>Creates a new dispel4py execution environment, <br>using the Kubernetes API. <br>Generates a new run directory, <br>stored under the user’s “runs” folder<br>(i.e. /<home>/<username>/runs/). <br>All the execution results are <br>stored in the generated run directory.</td>
			<td>application/json</td>
			<td>data (body), example: <br>{<br>
				"access_token": "string",<br>
				"workspace": "string",<br>
				"pckg": "string",<br>
				"pe_name":"string",<br>
				"target":"string",<br>
				"nodes":1<br>
				}
			</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/run-specfem/</td>
			<td>Deprecated endpoint. Use /run-cwl instead.</td>
			<td>application/json</td>
			<td>-</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/run-cwl/</td>
			<td>Endpoint to instantiate an execution environment <br>for CWL workflow execution. <br>The environment to be instantiated <br>is retrieved from the CWL using the <br>CWL Workflow Registry. Generates a new run <br>directory, stored under the user’s “runs” folder<br>(i.e. /<home>/<username>/runs/). All the execution <br>results are stored in the <br>generated run directory.</td>
			<td>application/json</td>
			<td>data (body), example:
				<br>{<br>
				"access_token": "string",<br>
				"nodes":12,<br>
				"workflow_name":"string",<br>
				"workflow_version": "string",<br>
				"input_data": <br>{<br>
				"example1":"string"<br>
				}<br>
				}
			</td>
		</tr>
		<tr>
			<td>POST</td>
			<td>/upload/</td>
			<td>Endpoint used to upload files in the DARE platform. <br>The files are stored under the user’s home directory. <br>The home directory is named after his/hers username and <br>inside there are 3 folders, i.e. uploads, debug and runs.<br>All the uploaded files are stored under the user’s “uploads” directory</td>
			<td>application/json</td>
			<td>data (body), example:
				<br>{<br>
				"dataset_name": "string",<br>
				"path": "string",<br>
				"access_token": "string",<br>
				"files": [<file list>]<br>
				}
			</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/my-files/</td>
			<td>Lists all the users’ directories under the “uploads”, <br>“runs” and “debug” folders. If the parameter <br>num_run_dirs is present, the response <br>is limited to the most recent directories based on <br>the number provided in the aforementioned parameter</td>
			<td>application/json</td>
			<td>-access_token(string)<br>-num_run_dirs(integer)</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/list/</td>
			<td>Lists all the files inside a specific directory. <br>This directory could be retrieved from the previous endpoint</td>
			<td>application/json</td>
			<td>-access_token(string)<br>-path(string)</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/download/</td>
			<td>Downloads a specific file from the DARE platform. <br>To find the file’s full path use the two previous endpoints</td>
			<td>application/json</td>
			<td>-access_token(string)<br>-path(string)</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/send2drop/</td>
			<td>Uploads files from the dare platform to B2DROP</td>
			<td>application/json</td>
			<td>-access_token(string)<br>-path(string)</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/cleanup/</td>
			<td>Clears the user’s folders (uploads, runs, debug)</td>
			<td>application/json</td>
			<td>-access_token(string)<br>-runs(boolean)<br>-uploads(boolean)<br>-debug(boolean)</td>
		</tr>
		<tr>
			<td>GET</td>
			<td>/my-pods</td>
			<td>List the running jobs of a user</td>
			<td>application/json</td>
			<td>data example: {"access_token": "string"}</td>
		</tr>
	</tbody>
</table>

Technical documentation of the component is also available [here](https://project-dare.gitlab.io/exec-api/)

## Provenance

**Version:** v1

### /data
---
##### ***GET***
**Description:** The data is selected by specifying a query string. Query parameters allow to search by attribution to a component or to an implementation, generation by a workflow execution and by combining more metadata and parameters terms with their min and max valuesranges. Mode of the search can also be indicated (mode ::= (OR | AND). It will apply to the search upon metadata and parameters values-ranges

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| usernames | query | csv list of users the Workflows Executons are associated with | No | string |
| terms | query | csv list of metadata or parameter terms. These relate positionally to the maxvalues and the minvalues | No | string |
| functionNames | query | csv list of functions the Data was generated with | No | string |
| wasAttributedTo | query | csv list of Component or Component Instances involved in the generation of the Data | No | string |
| minvalues | query | csv list of metadata or parameters minvalues. These relate positionally to the terms and the minvalues | No | string |
| rformat | query | unimplemented: format of the response payload (json,json-ld) | No | string |
| start | query | index of the starting item | Yes | integer |
| limit | query | max number of items expected | Yes | integer |
| maxvalues | query | csv list of metadata or parameters maxvalues. These relate positionally to the terms and the minvalues | No | string |
| formats | query | csv list of data formats (eg. mime-types) | No | string |
| types | query | csv list of data types | No | string |
| wasGeneratedBy | query | the id of the Invocation that generated the Data | No | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /data/filterOnAncestor
---
##### ***POST***
**Description:** Filter a list of data ids based on the existence of at least one ancestor in their data dependency graph, according to a list of metadata terms and their min and max values-ranges. Maximum depth level and mode of the search can also be indicated (mode ::= (OR | AND)

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body |  | No | object |

**Responses**

| Code | Description |
| ---- | ----------- |

### /data/{data_id}
---
##### ***GET***
**Description:** Extract Data and their DataGranules by the Data id

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| data_id | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /data/{data_id}/derivedData
---
##### ***GET***
**Description:** Starting from a specific data entity of the data dependency is possible to navigate through the derived data or backwards across the element's data dependencies. The number of traversal steps is provided as a parameter (level).

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| level | query | level of depth in the data derivation graph, starting from the current Data | Yes | string |
| data_id | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /data/{data_id}/export
---
##### ***GET***
**Description:** Export of provenance information PROV-XML or RDF format. The S-PROV information returned covers the whole workflow execution or is restricted to a single data element. In the latter case, the graph is returned by following the derivations within and across runs. A level parameter allows to indicate the depth of the resulting trace

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| format | query | export format of the PROV document returned | No | string |
| rdfout | query | export rdf format of the PROV document returned | No | string |
| creator | query | the name of the user requesting the export | No | string |
| level | query | level of depth in the data derivation graph, starting from the current Data | Yes | string |
| data_id | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /data/{data_id}/wasDerivedFrom
---
##### ***GET***
**Description:** Starting from a specific data entity of the data dependency is possible to navigate through the derived data or backwards across the element's data dependencies. The number of traversal steps is provided as a parameter (level).

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| level | query | level of depth in the data derivation graph, starting from the current Data | Yes | string |
| data_id | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /instances/{instid}
---
##### ***GET***
**Description:** Extract details about a single instance or component by specifying its id. The returning document will indicate the changes that occurred, reporting the first invocation affected. It support the specification of a list of runIds the instance was wasAssociateFor, considering that the same instance could be used across multiple runs

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| start | query | index of the starting item | Yes | integer |
| limit | query | max number of items expected | Yes | integer |
| wasAssociateFor | query | cvs list of runIds the instance was wasAssociateFor (when more instances are reused in multiple workflow executions) | No | string |
| instid | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /invocations/{invocid}
---
##### ***GET***
**Description:** Extract details about a single invocation by specifying its id

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| invocid | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /summaries/collaborative
---
##### ***GET***
**Description:** Extract information about the reuse and exchange of data between workflow executions based on terms' valuesranges and a group of users. The API method allows for inclusive or exclusive (mode ::= (OR j AND) queries on the terms' values. As above, additional details, such as running infrastructure, type and name of the workflow can be selectively extracted by assigning these properties to a groupBy parameter. This will support the generation of grouped views

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| wasAssociatedWith | query | csv lis of Components involved in the Workflow Executions | No | string |
| usernames | query | csv list of users the Workflows Executons are associated with | No | string |
| terms | query | csv list of metadata or parameter terms. These relate positionally to the maxvalues and the minvalues | No | string |
| functionNames | query | csv list of functions that are executed by at least one workflow's components | No | string |
| level | query | level of depth in the data derivation graph, starting from the current Data | Yes | string |
| minvalues | query | csv list of metadata or parameters minvalues. These relate positionally to the terms and the minvalues | No | string |
| rformat | query | unimplemented: format of the response payload (json,json-ld) | No | string |
| maxvalues | query | csv list of metadata or parameters maxvalues. These relate positionally to the terms and the minvalues | No | string |
| formats | query | csv list of data formats (eg. mime-types) | No | string |
| clusters | query | csv list of clusters that describe and group one or more workflow's component | No | string |
| groupby | query | express the grouping of the returned data | No | string |
| types | query |  | No | string |
| mode | query | execution mode of the workflow in case it support different kind of concrete mappings (eg. mpi, simple, multiprocess, etc.. | No | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /summaries/workflowexecution
---
##### ***GET***
**Description:** Produce a detailed overview of the distribution of the computation, reporting the size of data movements between the workflow components, their instances or invocations across worker nodes, depending on the specified granularity level. Additional information, such as process pid, worker, instance or component of the workflow (depending on the level of granularity) can be selectively extracted by assigning these properties to a groupBy parameter. This will support the generation of grouped views

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| level | query | level of depth in the data derivation graph, starting from the current Data | Yes | string |
| mintime | query | minimum start time of the Invocation | No | string |
| groupby | query | express the grouping of the returned data | No | string |
| runId | query | the id of the run to be analysed | No | string |
| maxtime | query | maximum start time of the Invocation | No | string |
| maxidx | query | maximum iteration index of an Invocation | No | integer |
| minidx | query | minimum iteration index of an Invocation | No | integer |

**Responses**

| Code | Description |
| ---- | ----------- |

### /terms
---
##### ***GET***
**Description:** Return a list of discoverable metadata terms based on their appearance for a list of runIds, usernames, or for the whole provenance archive. Terms are returned indicating their type (when consistently used), min and max values and their number occurrences within the scope of the search

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| aggregationLevel | query | set whether the terms need to be aggreagated by runId, username or across the whole collection (all) | No | string |
| usernames | query | csv list of usernames | No | string |
| runIds | query | csv list of run ids | No | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /workflowexecutions
---
##### ***GET***
**Description:** Extract documents from the bundle collection according to a query string which may include usernames, type of the workflow, the components the run wasAssociatedWith and their implementations. Data results' metadata and parameters can also be queried by specifying the terms and their min and max values-ranges and data formats. Mode of the search can also be indicated (mode ::= (OR j AND). It will apply to the search upon metadata and parameters values of each run

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| wasAssociatedWith | query | csv lis of Components involved in the Workflow Executions | No | string |
| usernames | query | csv list of users the Workflows Executons are associated with | No | string |
| terms | query | csv list of metadata or parameter terms. These relate positionally to the maxvalues and the minvalues | No | string |
| functionNames | query | csv list of functions that are executed by at least one workflow's components | No | string |
| minvalues | query | csv list of metadata or parameters minvalues. These relate positionally to the terms and the minvalues | No | string |
| rformat | query | unimplemented: format of the response payload (json,json-ld) | No | string |
| start | query | index of the starting item | Yes | integer |
| limit | query | max number of items expected | Yes | integer |
| maxvalues | query | csv list of metadata or parameters maxvalues. These relate positionally to the terms and the minvalues | No | string |
| formats | query | csv list of data formats (eg. mime-types) | No | string |
| clusters | query | csv list of clusters that describe and group one or more workflow's component | No | string |
| types | query |  | No | string |
| mode | query | execution mode of the workflow in case it support different kind of concrete mappings (eg. mpi, simple, multiprocess, etc.. | No | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /workflowexecutions/insert
---
##### ***POST***
**Description:** Bulk insert of bundle or lineage documents in JSON format. These must be provided as encoded stirng in a POST request

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body |  | No | object |

**Responses**

| Code | Description |
| ---- | ----------- |

### /workflowexecutions/import
---
##### ***POST***
**Description:** Import of provenance output which is not yet mapped to the s-ProvFlowMongoDB format. 
The files provided in the archive will be mapped to s-ProvFlowMongoDB if they are in one of the supported formats.

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| archive | form | Zip archive of provenance output, which will be mapped to s-ProvFlowMongoDB and stored. Currently only files in the CWLProv format are supported | Yes | file |
| format | form | Format of the provenance output to be imported. | Yes | String |


**Responses**

| Code | Description |
| ---- | ----------- |



### /workflowexecutions/{run_id}/export
---
##### ***GET***
**Description:** Export of provenance information PROV-XML or RDF format. The S-PROV information returned covers the whole workflow execution or is restricted to a single data element. In the latter case, the graph is returned by following the derivations within and across runs. A level parameter allows to indicate the depth of the resulting trace

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| rdfout | query | export rdf format of the PROV document returned | No | string |
| creator | query | the name of the user requesting the export | No | string |
| format | query | export format of the PROV document returned | No | string |
| run_id | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /workflowexecutions/{runid}
---
##### ***DELETE***
**Description:** Extract documents from the bundle collection by the runid of a WFExecution. The method will return input data and infomation about the components and the libraries used for the specific run

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| runid | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

##### ***GET***
**Description:** Extract documents from the bundle collection by the runid of a WFExecution. The method will return input data and infomation about the components and the libraries used for the specific run

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| runid | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /workflowexecutions/{runid}/delete
---
##### ***POST***
**Description:** Delete a workflow execution trace, including its bundle and all its lineage documents

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| runid | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /workflowexecutions/{runid}/edit
---
##### ***POST***
**Description:** Update of the description of a workflow execution. Users can improve this information in free-tex

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body |  | No | object |
| runid | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |

### /workflowexecutions/{runid}/showactivity
---
##### ***GET***
**Description:** Extract detailed information related to the activity related to a WFExecution (id). The result-set can be grouped by invocations, instances or components (parameter level) and shows progress, anomalies (such as exceptions or systems' and users messages), occurrence of changes and the rapid availability of accessible data bearing intermediate results. This method can also be used for runtime monitoring

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| start | query | index of the starting item | Yes | integer |
| limit | query | max number of items expected | Yes | integer |
| level | query | level of aggregation of the monitoring information (component, instance, invocation, cluster) | No | string |
| runid | path |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |


## Testing environment

The purpose of this component is to provide a DARE environment for test and debugging purposes. The component exposes two endpoints:

* The /playground endpoint: this simulates the dispel4py execution in DARE and prints the logs and output content directly to the user
* The /run-command endpoint: accepts any bash command, which is executed and returns the result to the user

### Use in notebook

* For the first endpoint, you need to execute the first steps as always: login, create workspace, register the workflow
* For the second endpoint, you need to provide the endpoint, the token, the command and the output file name if exists

### Update helper_functions

Add the below two methods in helper_functions:

* For the first endpoint
```python 
def debug_d4p(hostname, impl_id, pckg, workspace_id, pe_name, token, reqs=None, output_filename="output.txt",
              **kw):
    # Prepare data for posting
    data = {
        "impl_id": impl_id,
        "pckg": pckg,
        "wrkspce_id": workspace_id,
        "n_nodes": 1,
        "name": pe_name,
        "access_token": token,
        "output_filename": output_filename,
        "reqs": reqs if not (reqs is None) else "None"
    }
    d4p_args = {}
    for k in kw:
        d4p_args[k] = kw.get(k)
    data['d4p_args'] = d4p_args
    r = requests.post(hostname + '/playground', data=json.dumps(data))
    if r.status_code == 200:
        response = json.loads(r.text)
        if response["logs"]:
            print("Logs:\n========================")
            for log in response["logs"]:
                print(log)
        if response["output"]:
            print("Output content:\n==============================")
            for output in response["output"]:
                print(output)
    else:
        print('Playground returns status_code: \
                ' + str(r.status_code))
        print(r.text)
```

* For the second endpoint:
```python
import requests
import json

def exec_command(hostname, token, command, run_dir="new", output_filename="output.txt"):
    data = {
        "access_token": token,
        "command": command,
        "run_dir": run_dir,
        "output_filename": output_filename
    }

    r = requests.post(hostname + '/run-command', data=json.dumps(data))
    if r.status_code == 200:
        response = json.loads(r.text)
        if response["logs"]:
            print("Logs:\n========================")
            for log in response["logs"]:
                print(log)
        if response["output"]:
            print("Output content:\n==============================")
            for output in response["output"]:
                print(output)
        if response["run_dir"]:
            print("Run directory is: ")
            print(response["run_dir"])
    else:
        print('Playground returns status_code: \
                ' + str(r.status_code))
        print(r.text)
```

### Update the jupyter notebook

* For the /playground endpoint:

```python
F.debug_d4p(impl_id=impl_id, pckg="mysplitmerge_pckg", workspace_id=workspace_id, pe_name="mySplitMerge", 
            token=F.auth(), creds=creds, no_processes=6, iterations=1,
            reqs='https://gitlab.com/project-dare/dare-api/raw/master/examples/jupyter/requirements.txt')
```

* For the /run-command endpoint:

```python
    F.exec_command(PLAYGROUND_API_HOSTNAME, F.auth(), "pip install --user numpy")
```

Technical documentation of the component is also available [here](https://project-dare.gitlab.io/playground/)

## Semantic Data Discovery

The API documentation of the Semantic Data Discovery component is available in our [testbed environment](https://testbed.project-dare.eu/semantic-data)

# Dispel4py Documentation

dispel4py is a free and open-source Python library for describing abstract stream-based workflows for distributed data-intensive applications. It enables users to focus on their scientific methods, avoiding distracting details and retaining flexibility over the computing infrastructure they use.  It delivers mappings to diverse computing infrastructures, including cloud technologies, HPC architectures and  specialised data-intensive machines, to move seamlessly into production with large-scale data loads. The dispel4py system maps workflows dynamically onto multiple enactment systems, and supports parallel processing on distributed memory systems with MPI and shared memory systems with multiprocessing, without users having to modify their workflows.

## Dependencies

dispel4py has been tested with Python *2.7.6*, *2.7.5*, *2.7.2*, *2.6.6* and Python *3.4.3*, *3.6*, *3.7*.

The following Python packages are required to run dispel4py:

- networkx (https://networkx.github.io/)

If using the MPI mapping:

- mpi4py (http://mpi4py.scipy.org/)

## Installation

Clone this repository to your desktop. You can then install from the local copy to your python environment by calling:

```
python setup.py install
```

from the dispel4py root directory.

## Docker

The Dockerfile in the dispel4py root directory builds a Debian Linux distribution and installs dispel4py and OpenMPI.

```
docker build . -t dare-dispel4py
```

Start a Docker container with the dispel4py image in interactive mode with a bash shell:

```
docker run -it dare-dispel4py /bin/bash
```

For the EPOS use cases obspy is included in a separate Dockerfile `Dockerfile.seismo`:

```
docker build . -f Dockerfile.seismo -t dare-dispel4py-seismo
```

# Provenance in Dispel4py

lean_empty
-----------

.. code-block:: python

   clean_empty(d)

Utility function that given a dictionary in input, removes all the properties that are set to None.
It workes recursively through lists and nested documents

total_size
----------

.. code-block:: python

   total_size(o, handlers={}, verbose=False)

Returns the approximate memory footprint an object and all of its contents.

Automatically finds the contents of the following builtin containers and
their subclasses:  tuple, list, deque, dict, set and frozenset.
To search other containers, add handlers to iterate over their contents:

handlers = {SomeContainerClass: iter,
            OtherContainerClass: OtherContainerClass.get_elements}

write
-----

.. code-block:: python

   write(self, name, data)

Redefines the native write function of the dispel4py SimpleFunctionPE to take into account
provenance payload when transfering data.

getDestination_prov
-------------------

.. code-block:: python

   getDestination_prov(self, data)

When provenance is activated it redefines the native dispel4py.new.process getDestination function to take into account provenance information
when redirecting grouped operations.

commandChain
------------

.. code-block:: python

   commandChain(commands, envhpc, queue=None)

Utility function to execute a chain of system commands on the hosting oeprating system.
The current environment variable can be passed as parameter env.
The queue parameter is used to store the stdoutdata, stderrdata of each process in message

ProvenanceType
--------------

.. code-block:: python

   ProvenanceType(self)

A workflow is a program that combines atomic and independent processing elements
via a specification language and a library of components. More advanced systems
adopt abstractions to facilitate re-use of workflows across users'' contexts and application
domains. While methods can be multi-disciplinary, provenance
should be meaningful to the domain adopting them. Therefore, a portable specification
of a workflow requires mechanisms allowing the contextualisation of the provenance
produced. For instance, users may want to extract domain-metadata from a component
or groups of components adopting vocabularies that match their domain and current
research, tuning the level of granularity. To allow this level of flexibility, we explore
an approach that considers a workflow component described by a class, according to
the Object-Oriented paradigm. The class defines the behaviour of its instances as their
type, which specifies what an instance will do in terms of a set of methods. We introduce
the concept of *ProvenanceType*\ , that augments the basic behaviour by extending
the class native type, so that a subset of those methods perform the additional actions
needed to deliver provenance data. Some of these are being used by some of the preexisting
methods, and characterise the behaviour of the specific provenance type, some
others can be used by the developer to easily control precision and granularity. This approach,
tries to balance between automation, transparency and explicit intervention of the developer of a data-intensive tool, who
can tune provenance-awareness through easy-to-use extensions.

The type-based approach to provenance collection provides a generic *ProvenanceType* class
that defines the properties of a provenance-aware workflow component. It provides
a wrapper that meets the provenance requirements, while leaving the computational
behaviour of the component unchanged. Types may be developed as **Pattern Type** and **Contextual Type** to represent respectively complex
computational patterns and to capture specific metadata contextualisations associated to the produce output data.

The *ProvenanceType* presents the following class constants to indicate where the lineage information will be stored. Options include a remote
repository, a local file system or a *ProvenanceSensor* (experimental).


* \_SAVE_MODE\ *SERVICE='service'*
* \_SAVE_MODE\ *FILE='file'*
* \_SAVE_MODE\ *SENSOR='sensor'*

The following variables will be used to configure some general provenance capturing properties


* \_PROV\ *PATH*\ : When _SAVE_MODE\ *SERVICE* is chosen, this variable should be populated with a string indicating a file system path where the lineage will be stored.
* \_REPOS\ *URL*\ : When _SAVE_MODE\ *SERVICE* is chosen, this variable should be populated with a string indicating the repository endpoint (S-ProvFlow) where the provenance will be sent.
* \_PROV_EXPORT_URL: The service endpoint from where the provenance of a workflow execution, after being stored, can be extracted in PROV format.
* \_BULK\ *SIZE*\ : Number of lineage documents to be stored in a single file or in a single request to the remote service. Helps tuning the overhead brough by the latency of accessing storage resources.


getProvStateObjectId
^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.getProvStateObjectId(self, name)

Return the id of a named object stored in the provenance state

apply_derivation_rule
^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.apply_derivation_rule(self, event, voidInvocation, oport=None, iport=None, data=None, metadata=None)

In support of the implementation of a *ProvenanceType* realising a lineage *Pattern type*. This method is invoked by the *ProvenanceType* each iteration when a decision has to be made whether to ignore or discard the dependencies on the ingested stream
and stateful entities, applying a specific provenance pattern, thereby creating input/output derivations. The framework invokes this method every time the data is written on an output port (\ *event*\ : *write*\ ) and every
time an invocation (\ *s-prov:Invocation*\ ) ends (\ *event*\ : \_end_invocation\ *event*\ ). The latter can be further described by  the boolean parameter *voidInvocation*\ , indicating whether the invocation terminated with any data produced.
The default implementation provides a *stateless* behaviour, where the output depends only from the input data recieved during the invocation.

getInputAt
^^^^^^^^^^

.. code-block:: python

   ProvenanceType.getInputAt(self, port='input', index=None)

Return input data currently available at a specific *port*. When reading input of a grouped operator, the *gindex* parameter allows to access exclusively the data related to the group index.

addNamespacePrefix
^^^^^^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.addNamespacePrefix(self, prefix, url)

In support of the implementation of a *ProvenanceType* realising a lineage *Contextualisation type*.
A Namespace *prefix* can be declared with its vocabulary *url* to map the metadata terms to external controlled vocabularies.
They can be used to qualify the metadata terms extracted from the *extractItemMetadata* function,
as well as for those terms injected selectively at runtime by the *write* method. The namespaces will be used
consistently when exporting the lineage traces to semantic-web formats, such as RDF.

extractItemMetadata
^^^^^^^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.extractItemMetadata(self, data, port)

In support of the implementation of a *ProvenanceType* realising a lineage *Contextualisation type*.
Extracts metadata from the domain specific content of the data (s-prov:DataGranules) written on a components output *port*\ , according to a particular vocabulary.

ignorePastFlow
^^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.ignorePastFlow(self)

In support of the implementation of a *ProvenanceType* realising a lineage **Pattern type**.

It instructs the type to ignore the all the inputs when the method \_apply_derivation\ *rule* is invoked for a certain event."

ignoreState
^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.ignoreState(self)

In support of the implementation of a *ProvenanceType* realising a lineage **Pattern type**.

It instructs the type to ignore the content of the provenance state when the method \_apply_derivation\ *rule* is invoked for a certain event."

discardState
^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.discardState(self)

In support of the implementation of a *ProvenanceType* realising a lineage **Pattern type**.

It instructs the type to reset the data dependencies in the provenance state when the method \_apply_derivation\ *rule* is invoked for a certain event.
These will not be availabe in the following invocations."

discardInFlow
^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.discardInFlow(self, wlength=None, discardState=False)

In support of the implementation of a *ProvenanceType* realising a lineage **Pattern type**.

It instructs the type to reset the data dependencies related to the component''s inputs when the method \_apply_derivation\ *rule* is invoked for a certain event.
These will not be availabe in the following invocations."

update_prov_state
^^^^^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.update_prov_state(self, lookupterm, data, location='', format='', metadata={}, ignore_inputs=False, ignore_state=True, stateless=False, **kwargs)

In support of the implementation of a *ProvenanceType* realising a lineage *Pattern type* or inn those circumstances where developers require to explicitly manage the provenance information within the component''s logic,.

Updates the provenance state (\ *s-prov:StateCollection*\ ) with a reference, identified by a *lookupterm*\ , to a new *data* entity or to the current input. The *lookupterm* will allow developers to refer to the entity when this is used to derive new data.
Developers can specify additional *medatata* by passing a metadata dictionary. This will enrich the one generated by the *extractItemMetadata* method.
Optionally the can also specify *format* and *location* of the output when this is a concrete resource (file, db entry, online url), as well as instructing the provenance generation to 'ignore_input' and 'ignore_state' dependencies.

The *kwargs* parameter allows to pass an argument *dep* where developers can specify a list of data *id* to explicitly declare dependencies with any data in the provenance state (\ *s-prov:StateCollection*\ ).

write
^^^^^

.. code-block:: python

   ProvenanceType.write(self, name, data, **kwargs)

This is the native write operation of dispel4py triggering the transfer of data between adjacent
components of a workflow. It is extended by the *ProvenanceType* with explicit provenance
controls through the *kwargs* parameter. We assume these to be ignored
when provenance is deactivated. Also this method can use the lookup tags to
establish dependencies of output data on entities in the provenance state.

The *kwargs* parameter allows to pass the following arguments:


* *dep* : developers can specify a list of data *id* to explicitly declare dependencies with any data in the provenance state (\ *s-prov:StateCollection*\ ).
* *metadata*\ : developers can specify additional medatata by passing a metadata dictionary.
* _ignore\ *inputs*\ : instructs the provenance generation to ignore the dependencies on the current inputs.
* *format*\ : the format of the output.
* *location*\ : location of the output when this is a concrete resource (file, db entry, online url).

checkSelectiveRule
^^^^^^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.checkSelectiveRule(self, streammeta)

In alignement with what was previously specified in the configure_prov_run for the Processing Element,
check the data granule metadata whether its properies values fall in a selective provenance generation rule.

checkTransferRule
^^^^^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.checkTransferRule(self, streammeta)

In alignement with what was previously specified in the configure_prov_run for the Processing Element,
check the data granule metadata whether its properies values fall in a selective data transfer rule.

extractDataSourceId
^^^^^^^^^^^^^^^^^^^

.. code-block:: python

   ProvenanceType.extractDataSourceId(self, data, port)

In support of the implementation of a *ProvenanceType* realising a lineage *Pattern type*. Extract the id from the incoming data, if applicable,
to reuse it to identify the correspondent provenance entity. This functionality is handy especially when a workflow component ingests data represented by
self-contained and structured file formats. For instance, the NetCDF attributes Convention includes in its internal metadata an id that can be reused to ensure
the linkage and therefore the consistent continuation of provenance tracesbetween workflow executions that generate and use the same data.

AccumulateFlow
--------------

.. code-block:: python

   AccumulateFlow(self)

A *Pattern type* for a Processing Element (\ *s-prov:Component*\ ) whose output depends on a sequence of input data; e.g. computation of periodic average.

Nby1Flow
--------

.. code-block:: python

   Nby1Flow(self)

A *Pattern type* for a Processing Element (\ *s-prov:Component*\ ) whose output depends
on the data received on all its input ports in lock-step; e.g. combined analysis of multiple
variables.

SlideFlow
---------

.. code-block:: python

   SlideFlow(self)

A *Pattern type* for a Processing Element (\ *s-prov:Component*\ ) whose output depends
on computations over sliding windows; e.g. computation of rolling sums.

ASTGrouped
----------

.. code-block:: python

   ASTGrouped(self)

A *Pattern type* for a Processing Element (\ *s-prov:Component*\ ) that manages a stateful operator
with grouping rules; e.g. a component that produces a correlation matrix with the incoming
coefficients associated with the same sampling-iteration index

SingleInvocationFlow
--------------------

.. code-block:: python

   SingleInvocationFlow(self)

A *Pattern type* for a Processing Element (\ *s-prov:Component*\ ) that
presents stateless input output dependencies; e.g. the Processing Element of a simple I/O
pipeline.

AccumulateStateTrace
--------------------

.. code-block:: python

   AccumulateStateTrace(self)

A *Pattern type* for a Processing Element (\ *s-prov:Component*\ ) that
keeps track of the updates on intermediate results written to the output after a sequence
of inputs; e.g. traceable approximation of frequency counts or of periodic averages.

IntermediateStatefulOut
-----------------------

.. code-block:: python

   IntermediateStatefulOut(self)

A *Pattern type* for a Processing Element (\ *s-prov:Component*\ ) stateful component which produces distinct but interdependent
output; e.g. detection of events over periodic observations or any component that reuses the data just written to generate a new product

ForceStateless
--------------

.. code-block:: python

   ForceStateless(self)

A *Pattern type* for a Processing Element (\ *s-prov:Component*\ ). It considers the outputs of the component dependent
only on the current input data, regardless from any explicit state update; e.g. the user wants to reduce the
amount of lineage produced by a component that presents inline calls to the \_update_prov\ *state*\ , accepting less accuracy.

get_source
----------

.. code-block:: python

   get_source(object, spacing=10, collapse=1)

Print methods and doc strings.
Takes module, class, list, dictionary, or string.

injectProv
----------

.. code-block:: python

   injectProv(object, provType, active=True, componentsType=None, workflow={}, **kwargs)

This function dinamically extend the type of each the nodes of the graph
or subgraph with ProvenanceType type or its specialisation.

configure_prov_run
------------------

.. code-block:: python

   configure_prov_run(graph, provRecorderClass=None, provImpClass=<class 'dispel4py.provenance.ProvenanceType'>, input=None, username=None, workflowId=None, description=None, system_id=None, workflowName=None, workflowType=None, w3c_prov=False, runId=None, componentsType=None, clustersRecorders={}, feedbackPEs=[], save_mode='file', sel_rules={}, transfer_rules={}, update=False, sprovConfig=None, sessionId=None, mapping='simple')

In order to enable the user of a data-intensive application to configure the lineage metadata extracted from the execution of their
worklfows we adopt a provenance configuration profile. The configuration is used at the time of the initialisation of the workflow to prepare its provenance-aware
execution. We consider that a chosen configuration may be influenced by personal and community preferences, as well as by rules introduced by institutional policies.
For instance, a certain RI would require to choose among a set of contextualisation types, in order to adhere to
the infrastructure's metadata portfolio. Thus, a provenance configuration profile play
in favour of more generality, encouraging the implementation and the re-use of fundamental
methods across disciplines.

With this method, the users of the workflow provide general provenance information on the attribution of the run, such as *username*\ , *runId* (execution id),
*description*\ , *workflowName*\ , and its semantic characterisation *workflowType*. It allows users to indicate which provenance types to apply to each component
and the belonging conceptual provenance cluster. Moreover, users can also choose where to store the lineage (\_save\ *mode*\ ), locally in the file system or in a remote service or database.
Lineage storage operations can be performed in bulk, with different impacts on the overall overhead and on the experienced rapidity of access to the lineage information.


* **Configuration JSON**\ : We show here an example of the JSON document used to prepare a worklfow for a provenance aware execution. Some properties are described inline. These are defined by terms in the provone and s-prov namespaces.

.. code-block:: python

       {
               'provone:User': "aspinuso",
               's-prov:description' : "provdemo demokritos",
               's-prov:workflowName': "demo_epos",
               # Assign a generic characterisation or aim of the workflow
               's-prov:workflowType': "seis:preprocess",
               # Specify the unique id of the workflow
               's-prov:workflowId'  : "workflow process",
               # Specify whether the lineage is saved locally to the file system or remotely to an existing serivce (for location setup check the class prperties or the command line instructions section.)
               's-prov:save-mode'   : 'service'         ,
               # Assign the Provenance Types and Provenance Clusters to the processing elements of the workflows. These are indicated by the name attributed to their class or function, eg. PE_taper. The 's-prov:type' property accepts a list of class names, corrisponding to the types' implementation. The 's-prov:cluster' is used to group more processing elements to a common functional section of the workflow.
               's-prov:componentsType' :
                                  {'PE_taper': {'s-prov:type':["SeismoPE"]),
                                                's-prov:prov-cluster':'seis:Processor'},
                                   'PE_plot_stream':    {'s-prov:prov-cluster':'seis:Visualisation',
                                                      's-prov:type':["SeismoPE"]},
                                   'StoreStream':    {'s-prov:prov-cluster':'seis:DataHandler',
                                                      's-prov:type':["SeismoPE,AccumulateFlow"]}
                                   }}


* **Selectivity rules**\ : By declaratively indicating a set of Selectivity rules for every component ('s-prov:sel_rules'), users can respectively activate the collection
  of the provenance for particular Data elements or trigger transfer operations of the data to external locations. The approach takes advantage of the contextualisation
  possibilities offered by the provenance *Contextualisation types*. The rules consist of comparison expressions formulated in JSON that indicate the boundary
  values for a specific metadata term. Such representation is inspired by the query language and selectors adopted by a popular document store, MongoDB.
  These can be defined also within the configuration JSON introduced above.

Example, a Processing Element *CorrCoef* that produces lineage information only when the *rho* value is greater than 0:

.. code-block:: python

       { "CorrCoef": {
           "rules": {
               "rho": {
                   "$gt": 0
       }}}}


* ** Command Line Activation**\ : To enable proveance activation through command line dispel4py should be executed with specific command line instructions. The following command will execute a local test for the provenance-aware execution of the MySplitAndMerge workflow.**

.. code-block:: python

   dispel4py --provenance-config=dispel4py/examples/prov_testing/prov-config-mysplitmerge.json --provenance-repository-url=<url> multi dispel4py/examples/prov_testing/mySplitMerge_prov.py -n 10

* The following command instead stores the provenance files to the local filesystem in a given directory. To activate this mode, the property *s-prov:save_mode* of the configuration file needs to be set to 'file'.

.. code-block:: python

    dispel4py --provenance-config=dispel4py/examples/prov_testing/prov-config-mysplitmerge.json --provenance-path=/path/to/prov multi dispel4py/examples/prov_testing/mySplitMerge_prov.py -n 10
    
    
ProvenanceSimpleFunctionPE
--------------------------

.. code-block:: python

   ProvenanceSimpleFunctionPE(self, *args, **kwargs)

A *Pattern type* for the native  *SimpleFunctionPE* of dispel4py

ProvenanceIterativePE
---------------------

.. code-block:: python

   ProvenanceIterativePE(self, *args, **kwargs)

A *Pattern type* for the native  *IterativePE* Element of dispel4py
