#!/usr/bin/bash

echo "Getting port for Login Service"
LOGIN_PORT=$(kubectl get svc  | grep "dare-login-public"  | awk '{print $5}' | sed "s/.*://" | sed "s/\/TCP//")
echo "${LOGIN_PORT}"
echo "Getting the port for the CWL workflow registry"
WORKFLOW_PORT=$(kubectl get svc  | grep "workflow-registry-public"  | awk '{print $5}' | sed "s/.*://" | sed "s/\/TCP//")
echo "${WORKFLOW_PORT}"
echo "Getting the port for the Execution API"
EXEC_PORT=$(kubectl get svc  | grep "exec-api-public"  | awk '{print $5}' | sed "s/.*://" | sed "s/\/TCP//")
echo  "${EXEC_PORT}"