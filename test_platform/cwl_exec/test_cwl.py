import json
from os import getcwd, mkdir
from os.path import join, exists

import requests

import helper_functions as hf

hostname = "https://platform.dare.scai.fraunhofer.de/workflow-registry"
login_hostname = "https://platform.dare.scai.fraunhofer.de/dare-login"
execution_hostname = "https://platform.dare.scai.fraunhofer.de/exec-api"

success = 0
fail = 0


def update_counts(status_code, success, fail):
    if status_code == 200:
        success += 1
    else:
        fail += 1
    return success, fail


if __name__ == '__main__':
    credentials = hf.read_credentials()
    auth_token = hf.login(hostname=login_hostname, username=credentials["username"], password=credentials["password"],
                          requested_issuer=credentials["issuer"])
    if credentials["ip"]:
        if credentials["workflow_port"]:
            hostname = "http://{}/{}".format(credentials["ip"], credentials["workflow_port"])
        if credentials["login_port"]:
            login_hostname = "http://{}/{}".format(credentials["ip"], credentials["login_port"])
        if credentials["exec_port"]:
            execution_hostname = "http://{}/{}".format(credentials["ip"], credentials["exec_port"])

    print("Auth token: {}".format(auth_token))
    if auth_token and type(auth_token) == dict:
        success += 1
        token = auth_token["access_token"]

        print("Registering the docker env")
        docker_folder = join(getcwd(), "docker_files")
        if not exists(docker_folder):
            mkdir(docker_folder)

        # get the latest docker files
        dockerfile = requests.get(
            "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-f4f/Dockerfile")
        with open(join(docker_folder, "Dockerfile"), "w") as f:
            f.write(dockerfile.text)

        entrypoint = requests.get(
            "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-f4f/entrypoint.sh")
        with open(join(docker_folder, "entrypoint.sh"), "w") as f:
            f.write(entrypoint.text)

        cwl_files_script = requests.get(
            "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-f4f/cwl-files.py")
        with open(join(docker_folder, "cwl-files.py"), "w") as f:
            f.write(cwl_files_script.text)

        script_names = ["entrypoint.sh", "cwl-files.py"]
        docker_env = hf.create_docker_env_with_scripts(hostname=hostname, token=token, docker_name="f4f",
                                                       docker_tag="v1.0", script_names=script_names,
                                                       path_to_files=docker_folder)

        print(docker_env[1])
        success, fail = update_counts(docker_env[0], success, fail)
        if "Duplicate entry" in docker_env[1]:
            success += 1
            fail -= 1
        print("Updating the docker url with a public image")
        url = "registry.gitlab.com/project-dare/dare-platform/exec-context-f4f:v1.0"
        resp = hf.provide_url(hostname=hostname, token=token, docker_name="f4f", docker_tag="v1.0",
                              docker_url=url)
        print(resp[1])
        success, fail = update_counts(resp[0], success, fail)

        print("Registering the workflow")

        cwl_folder = join(getcwd(), "cwl_files")
        if not exists(cwl_folder):
            mkdir(cwl_folder)

        # get the workflow
        workflow = requests.get("https://gitlab.com/project-dare/exec-api/-/raw/master/examples/f4f/cwl/analytics.cwl")
        with open(join(cwl_folder, "analytics.cwl"), "w") as f:
            f.write(workflow.text)

        # get the spec file
        spec = requests.get("https://gitlab.com/project-dare/exec-api/-/raw/master/examples/f4f/cwl/spec.yaml")
        with open(join(cwl_folder, "spec.yaml"), "w") as f:
            f.write(spec.text)

        # get the python script for the execution
        python_script = requests.get(
            "https://gitlab.com/project-dare/exec-api/-/raw/master/examples/f4f/cwl/analytics.py")
        with open(join(cwl_folder, "analytics.py"), "w") as f:
            f.write(python_script.text)

        workflow_part_data = [{"name": "analytics.py", "version": "v1.0"}]

        workflow = hf.create_workflow(hostname=hostname, token=token, workflow_name="analytics.cwl",
                                      workflow_version="v1.0", spec_name="spec.yaml", path_to_cwls=cwl_folder,
                                      docker_name="f4f", docker_tag="v1.0", workflow_part_data=workflow_part_data)
        print(workflow[1])
        success, fail = update_counts(workflow[0], success, fail)
        if "Duplicate entry" in docker_env[1]:
            success += 1
            fail -= 1

        print("Execute the CWL workflow")
        try:
            input_params = "input_example_cwl.json"
            with open(input_params, "r") as f:
                input_data = json.loads(f.read())

            resp = hf.submit_cwl(hostname=execution_hostname, token=token, workflow_name="analytics.cwl",
                                 workflow_version="v1.0",
                                 input_data=input_data)
            print(resp)
            success += 1
            print("Monitoring the containers")
            print("When there are no more containers, use the test_exec.py to find the output")
            print("Copy the run_dir returned by the execution and update the variable run_dir in the test_exec.py")
            hf.monitor(token, execution_hostname)
            print("Get the run dir from here: {}".format(resp))
        except (BaseException, Exception):
            fail += 1
        print("Tests passed: {}".format(success))
        print("Tests failed: {}".format(fail))
    else:
        print("Could not login! No more tests can be executed")
        exit(-1)
