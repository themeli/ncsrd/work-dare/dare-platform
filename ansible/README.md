
## Synchronize 3d-party repositories
```
git submodule update --init --recursive
```

## Spawn a Vagrant VM to install ansible
```
vagrant up
vagrant ssh
```


## Create SSH Keys to access nodes (optional).
```
ssh-keygen -t rsa
ssh-copy-id 
```

```
ansible-playbook -i inventory/hosts.ini --become --become-user=root --become-method=sudo --ask-sudo-pass -u user --extra-vars bootstrap_os=ubuntu cluster.yml
```
