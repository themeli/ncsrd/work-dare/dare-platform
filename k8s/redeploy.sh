#!/bin/bash
for i in `ls | grep "dare-login*" `;do kubectl delete -f $i;done
kubectl delete -f d4p-registry-dp.yaml
kubectl delete -f d4p-registry-svc.yaml
# kubectl delete -f prov-queue-dp.yaml
# kubectl delete -f prov-queue-svc.yaml
# kubectl delete -f sprov-dp.yaml
# kubectl delete -f sprov-svc.yaml
# kubectl delete -f sprov-view-dp.yaml
# kubectl delete -f sprov-view-svc.yaml
# for i in `ls | grep "rbac*" `;do kubectl delete -f $i;done
for i in `ls | grep "exec-api*" `;do kubectl delete -f $i;done
for i in `ls | grep "playground*"`;do kubectl delete -f $i;done
# for i in `ls | grep "dare-ingress*" `;do kubectl delete -f $i;done
# for i in `ls | grep "keycloak-ingress*" `;do kubectl delete -f $i;done
# for i in `ls | grep "solr*" `;do kubectl delete -f $i;done
# for i in `ls | grep "semantic-data*" `;do kubectl delete -f $i;done
kubectl delete -f workflow-registry-dp.yaml
kubectl delete -f workflow-registry-svc.yaml

for i in `ls | grep "dare-login*" `;do kubectl create -f $i;done
kubectl create -f d4p-registry-dp.yaml
kubectl create -f d4p-registry-svc.yaml
# kubectl create -f prov-queue-dp.yaml
# kubectl create -f prov-queue-svc.yaml
# kubectl create -f sprov-dp.yaml
# kubectl create -f sprov-svc.yaml
# kubectl create -f sprov-view-dp.yaml
# kubectl create -f sprov-view-svc.yaml
for i in `ls | grep "exec-api*" `;do kubectl create -f $i;done
# for i in `ls | grep "rbac*" `;do kubectl create -f $i;done
for i in `ls | grep "playground*"`;do kubectl create -f $i;done
# for i in `ls | grep "dare-ingress*" `;do kubectl create -f $i;done
# for i in `ls | grep "keycloak-ingress*" `;do kubectl create -f $i;done
# for i in `ls | grep "semantic-data*" `;do kubectl create -f $i;done
kubectl create -f workflow-registry-dp.yaml
kubectl create -f workflow-registry-svc.yaml
