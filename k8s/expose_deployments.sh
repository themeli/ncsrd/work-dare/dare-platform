#!/bin/bash

kubectl expose deployment d4p-registry --type=NodePort --name=d4p-registry-public
kubectl expose deployment dare-login --type=NodePort --name=dare-login-public
kubectl expose deployment exec-api --type=NodePort --name=exec-api-public
kubectl expose deployment playground --type=NodePort --name=playground-public
kubectl expose deployment semantic-data --type=NodePort --name=semantic-data-public
kubectl expose deployment workflow-registry --type=NodePort --name=workflow-registry-public
